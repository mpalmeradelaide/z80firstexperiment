library ieee; 

use ieee.numeric_std.all;
use ieee.std_logic_1164.all;

entity top is
port(
    lonely_led	:	out	std_logic
);	
end entity;

ARCHITECTURE rtl OF top IS
	-- SB_HFOSC for the clock
	component SB_HFOSC is
	generic(
		CLKHF_DIV: string:="0b00"
	);
	port(
	   	CLKHF : out std_logic;
		CLKHFEN  :in std_logic;
		CLKHFPU : in std_logic
	);
	end component;
	
	-- T80se
	COMPONENT T80se
	PORT(
		RESET_n		:	IN		std_logic;
		CLK_n		:	IN		std_logic;
		CLKEN		:	IN		std_logic;
		WAIT_n		:	IN		std_logic;
		INT_n		:	IN		std_logic;
		NMI_n		:	IN		std_logic;
		BUSRQ_n		:	IN		std_logic;
		DI			:	IN		std_logic_vector(7 downto 0);          
		M1_n		:	OUT		std_logic;
		MREQ_n		:	OUT		std_logic;
		IORQ_n		:	OUT		std_logic;
		RD_n		:	OUT		std_logic;
		WR_n		:	OUT		std_logic;
		RFSH_n		:	OUT		std_logic;
		HALT_n		:	OUT		std_logic;
		BUSAK_n		:	OUT		std_logic;
		A			:	OUT		std_logic_vector(15 downto 0);
		DO			:	OUT		std_logic_vector(7 downto 0)
		);
	END COMPONENT;
	
	-- bootstrap mem
	COMPONENT boot_mem
	PORT(
		WADDR : IN std_logic_vector(8 downto 0);
		WDATA : IN std_logic_vector(7 downto 0);
		WE : IN std_logic;
		WCLKE : IN std_logic;
		WCLK : IN std_logic;
		RADDR : IN std_logic_vector(8 downto 0);
		RE : IN std_logic;
		RCLKE : IN std_logic;
		RCLK : IN std_logic;          
		RDATA : OUT std_logic_vector(7 downto 0)
		);
	END COMPONENT;
    
	signal	n_rst	:	std_logic;
	signal	clk		:	std_logic;

	-- data/address
	signal	data_from_z80		:	std_logic_vector(7 downto 0);
	signal	address_from_z80	:	std_logic_vector(15 downto 0);
	signal	data_to_z80			:	std_logic_vector(7 downto 0);
	
	-- signals from t80
	signal	wr_n	:	std_logic;
	signal	mreq_n	:	std_logic;
	signal	iorq_n	:	std_logic;
	signal	halt_n	:	std_logic;
	signal	m1_n	:	std_logic;
	signal	rd_n	:	std_logic;
	signal	rfsh_n	:	std_logic;
	signal	busak_n	:	std_logic;
	
	-- glue logic
	signal	read_mem	:	std_logic;
	signal	write_mem	:	std_logic;
	signal	read_port	:	std_logic;
	signal	write_port	:	std_logic;
	
	-- register
	signal	led_reg		:	std_logic;
	signal	led_next	:	std_logic;
	
BEGIN
                 
	-- don't reset
	n_rst <= '1';
	
	iosc: SB_HFOSC
	generic map(
		CLKHF_DIV => "0b11")
	port map(
		CLKHFPU => '1',	--Tie pullup high
		CLKHFEN => '1',	--Enable clock output
		CLKHF => clk	--Clock output
	);
	
	-- Z80 clone
	z80_unit: T80se
	PORT MAP(
		RESET_n		=> n_rst,
		CLK_n		=> clk,
		CLKEN		=> '1',
		WAIT_n		=> '1',
		INT_n		=> '1',
		NMI_n		=> '1',
		BUSRQ_n		=> '1',
		M1_n		=> m1_n,
		MREQ_n		=> mreq_n,
		IORQ_n		=> iorq_n,
		RD_n		=> rd_n,
		WR_n		=> wr_n,
		RFSH_n		=> rfsh_n,
		HALT_n		=> halt_n,
		BUSAK_n		=> busak_n,
		A			=> address_from_z80,
		DI			=> data_to_z80,
		DO			=> data_from_z80
	);

	-- glue logic
	read_mem <= (not mreq_n) and (not rd_n);
	write_mem <= (not mreq_n) and (not wr_n);
	read_port <= (not iorq_n) and (not rd_n) and mreq_n;
	write_port <= (not iorq_n) and (not wr_n) and mreq_n;
	
	boot_mem_unit: boot_mem PORT MAP(
		-- write address input
		WADDR => address_from_z80(8 downto 0),
		-- write data input
		WDATA => data_from_z80,
		-- enable write input
		WE => write_mem,
		-- write clock enable
		WCLKE => '1',
		-- write clock
		WCLK => clk,
		-- read address
		RADDR => address_from_z80(8 downto 0),
		-- read data output
		RDATA => data_to_z80,
		-- read enable
		--RE => read_port,
		RE => '1',
		-- read clock enable
		RCLKE => '1',
		-- read clock
		RCLK => clk
	);
	
	
	-- registers
	process(clk, n_rst)
	begin
		if(n_rst = '0') then
			led_reg <= '0';
		elsif (clk'event and clk='1') then
		    led_reg <= led_next;
		end if;
	end process;
	
	-- handle output to ports
	process(clk, write_port, led_reg, address_from_z80, data_from_z80)
	begin
		led_next <= led_reg;
		
		if (write_port = '1') then
			-- lonely led port address 0x01 for simplicity
			if (address_from_z80(7 downto 0) = X"01") then
				if(data_from_z80 /= X"00") then
					led_next <= '1';
				else
					led_next <= '0';
				end if;
			end if;
		end if; 
	
	end process;
		
	-- lonely_led get output from port reg
	lonely_led <= led_reg;
	--lonely_led <= address_from_z80(0);

END ARCHITECTURE rtl;
