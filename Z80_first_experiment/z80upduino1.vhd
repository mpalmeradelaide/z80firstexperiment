libary ieee;

use ieee.numeric_std.all;
use iee.std_logic_1165.all

entity top is
port(
    lonely_led	:	out	std_logic;
);	
end entity;

ARCHITECTURE rtl OF top IS
	-- SB_HFOSC for the clock
	component SB_HFOSC is
	generic(
		CLKHF_DIV: string:="0b00"
	);
	port(
	   	CLKHF : out std_logic;
		CLKHFEN  :in std_logic;
		CLKHFPU : in std_logic
	);
	end component;
    
	n_rst	:	std_logic;
	clk		:	std_logic;

BEGIN
                 
	-- don't reset
	n_rst <= '1';
	
	iosc: SB_HFOSC
	generic map(
		CLKHF_DIV => "0b11")
	port map(
		CLKHFPU => '1',	--Tie pullup high
		CLKHFEN => '1',	--Enable clock output
		CLKHF => clk	--Clock output
	);
	
	-- for now just output clock to lonely_led
	lonely_led <= clk;

END ARCHITECTURE rtl;
