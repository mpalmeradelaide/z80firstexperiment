
#Begin clock constraint
define_clock -name {boot_mem|WCLK} {p:boot_mem|WCLK} -period 10000000.000 -clockgroup Autoconstr_clkgroup_0 -rise 0.000 -fall 5000000.000 -route 0.000 
#End clock constraint

#Begin clock constraint
define_clock -name {boot_mem|RCLK} {p:boot_mem|RCLK} -period 10000000.000 -clockgroup Autoconstr_clkgroup_1 -rise 0.000 -fall 5000000.000 -route 0.000 
#End clock constraint
