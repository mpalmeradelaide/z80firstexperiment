library ieee; 

use ieee.numeric_std.all;
use ieee.std_logic_1164.all;

entity boot_mem is
port(
    WADDR  : in	std_logic_vector(8 downto 0);
  	WDATA  : in std_logic_vector(7 downto 0);
  	WE	   : in std_logic;
  	WCLKE  : in std_logic;
  	WCLK   : in std_logic;
  	RADDR  : in std_logic_vector(8 downto 0);
  	RDATA  : out std_logic_vector(7 downto 0);
  	RE     : in std_logic;
  	RCLKE  : in std_logic;
  	RCLK   : in std_logic
);
end entity;

ARCHITECTURE boot_mem_arch OF boot_mem IS
  -- boot_mem
  
  constant	ADDR_WIDTH: integer := 5;
  constant	DATA_WIDTH: integer := 8;
  type rom_type is array (0 to 2**ADDR_WIDTH-1) of std_logic_vector(DATA_WIDTH-1 downto 0);
  -- rom definition
  constant BOOT_ROM_CODE: rom_type:=(
  X"3E",
  X"FF",
  X"0E",
  X"01",
  X"ED",
  X"79",
  X"C3",
  X"06",
  X"00",
  X"00",
  X"00",
  X"00",
  X"00",
  X"00",
  X"00",
  X"00",
  X"00",
  X"00",
  X"00",
  X"00",
  X"00",
  X"00",
  X"00",
  X"00",
  X"00",
  X"00",
  X"00",
  X"00",
  X"00",
  X"00",
  X"00",
  X"00"
  );
  
BEGIN
		
	 RDATA <= BOOT_ROM_CODE(to_integer(unsigned(RADDR(5 downto 0))));
 
	      
END ARCHITECTURE boot_mem_arch;
